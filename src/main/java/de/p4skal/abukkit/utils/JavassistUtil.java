package de.p4skal.abukkit.utils;

import javassist.*;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Set;

/**
 * Util for all Javassist Stuff.
 *
 * @author p4skal
 */
public class JavassistUtil {

    /**
     * Get Java Method from Javassist Method.
     *
     * @param ctClass  To find Method.
     * @param ctMethod To transform to Java.
     * @return Java Method.
     * @throws NotFoundException      Called when Javassist can't find the Class.
     * @throws CannotCompileException Called when Javassist can't transform CtClass to Class.
     * @throws NoSuchMethodException  Cloud not find Method.
     */
    public Method getJavaMethod( CtClass ctClass, CtMethod ctMethod ) throws NotFoundException, CannotCompileException,
            NoSuchMethodException {
        // Create Parameter Array.
        Class<?>[] parameters = new Class[ctMethod.getParameterTypes().length];

        // Iterate Method Parameters.
        for ( int i = 0; i < ctMethod.getParameterTypes().length; i++ ) {
            // Map Javassist Parameter to Java Parameter.
            parameters[i] = ctMethod.getParameterTypes()[i].toClass();
        }

        // Get Java Class.
        Class<?> clazz = ctClass.toClass();

        return clazz.getDeclaredMethod( ctClass.getName(), parameters );
    }

    /**
     * Get Java Method from Javassist Method.
     *
     * @param ctClass To find Method.
     * @param ctField To transform to Java.
     * @return Java Method.
     * @throws NoSuchFieldException   Called when Java can't find the Field.
     * @throws CannotCompileException Called when Javassist can't transform CtClass to Class.
     */
    public Field getJavaField( CtClass ctClass, CtField ctField ) throws NoSuchFieldException, CannotCompileException {
        return ctClass.toClass().getDeclaredField( ctField.getName() );
    }

    /**
     * Recursively find all (nested) Classes.
     *
     * @param ctClasses To add all Classes.
     * @param ctClass   To inspect.
     * @throws NotFoundException Called when Javassist can't find the Class.
     */
    public void getNestedClasses( Set<CtClass> ctClasses, CtClass ctClass ) throws NotFoundException {
        // Add current Class.
        ctClasses.add( ctClass );

        // Iterate nested Classes.
        for ( CtClass aClass : ctClass.getNestedClasses() ) {
            // Invoke this Method for every nested Class.
            this.getNestedClasses( ctClasses, aClass );
        }

    }
}
