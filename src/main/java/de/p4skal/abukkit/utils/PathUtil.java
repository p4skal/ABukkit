package de.p4skal.abukkit.utils;

import de.p4skal.abukkit.ABukkit;
import lombok.Getter;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

/**
 * Util for Plugin finding.
 *
 * @author p4skal
 */
@Getter
public class PathUtil {
    private final ABukkit aBukkit;
    /**
     * Root Directory of Bukkit Server. (Contain bukkit.jar, plugins etc.)
     */
    private final File bukkitFolder;
    /**
     * Contains every APlugin as Jar File.
     */
    private File[] rawPlugins;
    /**
     * Contains every Library as Jar File.
     */
    private File[] libraries;

    /**
     * Constructor of Path Util
     *
     * @param aBukkit The ABukkit MainClass.
     */
    public PathUtil( ABukkit aBukkit ) {
        this.aBukkit = aBukkit;
        this.bukkitFolder = aBukkit.getDataFolder().getParentFile();
        try {
            this.rawPlugins = this.getJars( "aplugins" );
            this.libraries = this.getJars( "libraries" );
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Get all Jars inside the Folder.
     *
     * @return Java File Array containing all Jars.
     * @throws IOException Failed to create Folder.
     */
    private File[] getJars( String folder ) throws IOException {
        // Get ABukkit Plugin Folder.
        File file = new File( this.getBukkitFolder(), folder + "/" );

        // Check if DataFolder exist.
        if ( !file.exists() ) {
            // Create DataFolder.
            if ( !file.mkdirs() ) {
                // Throw Exception on failure.
                throw new IOException( "Can't create " + folder + " Folder!" );
            }
        }

        // Get and return ABukkit Plugins.
        return file.listFiles( new FilenameFilter() {
            public boolean accept( File dir, String name ) {
                return name.toLowerCase().endsWith( "jar" );
            }
        } );
    }
}
