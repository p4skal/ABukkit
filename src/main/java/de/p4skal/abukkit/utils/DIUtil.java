package de.p4skal.abukkit.utils;

import gnu.trove.map.TMap;
import gnu.trove.map.hash.THashMap;
import lombok.Getter;

import java.util.Map;

/**
 * Dependency Injection Util. Contain Injection Pool etc.
 *
 * @author p4skal
 */
@Getter
public class DIUtil {
    /**
     * Contain all injected Singletons referenced by Class.
     */
    private TMap<Class<?>, Object> injectionPool = new THashMap<>();

    /**
     * Get Object from Injection Pool which Class is Superclass, or equal to lower Class.
     *
     * @param lowerClass Can be down casted from super Class in Pool.
     * @return First matching instance.
     */
    public Object getAssignableFrom( Class<?> lowerClass ) {
        // Iterate Injection Pool.
        for ( Map.Entry<Class<?>, Object> entry : this.getInjectionPool().entrySet() ) {
            // Check if Key is assignable from given Class.
            if ( entry.getKey().isAssignableFrom( lowerClass ) ) {
                return entry.getValue();
            }
        }
        return null;
    }

    /**
     * Get Object from Injection Pool which Class is lower Class, or equal to given super Class.
     *
     * @param superClass Can be up casted to lower Class in Pool.
     * @return First matching instance.
     */
    public Object getAssignableTo( Class<?> superClass ) {
        // Iterate Injection Pool.
        for ( Map.Entry<Class<?>, Object> entry : this.getInjectionPool().entrySet() ) {
            // Check if Key is assignable from given Class.
            if ( superClass.isAssignableFrom( entry.getKey() ) ) {
                return entry.getValue();
            }
        }
        return null;
    }

}
