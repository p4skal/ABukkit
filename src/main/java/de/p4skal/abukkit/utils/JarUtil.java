package de.p4skal.abukkit.utils;

import de.p4skal.abukkit.ABukkit;
import gnu.trove.set.hash.THashSet;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Util for Jar loading.
 *
 * @author p4skal
 */
@Getter
@RequiredArgsConstructor
public class JarUtil {
    private final ABukkit aBukkit;

    /**
     * Get all Classes and nested Classes in File.
     *
     * @param classPool To load Class.
     * @param file To get Classes from.
     * @return Javassist Classes as Array.
     * @throws IOException Called when Javassist can't find Plugin File.
     * @throws NotFoundException Called when Plugin File is no Jar File.
     */
    public CtClass[] getClasses( ClassPool classPool, File file ) throws IOException, NotFoundException {
        // Create Class Set.
        Set<CtClass> classes = new THashSet<>();

        // Get Classes.
        Enumeration<JarEntry> entries = new JarFile( file ).entries();

        // Iterate Entries.
        while ( entries.hasMoreElements() ) {
            // Get Entry.
            JarEntry entry = entries.nextElement();

            // Check for Package / other.
            if ( entry.isDirectory() || !entry.getName().endsWith( ".class" ) ) {
                // Skip if not class.
                continue;
            }

            // Path to Class.
            String name = entry.getName().replace( '/', '.' );

            // Get Javassist Class and add to Classes.
            classes.add( classPool.get( name ) );
        }

        // Iterate Classes.
        for ( CtClass ctClass : classes ) {
            // Try to find nested Classes.
            this.getABukkit().getJavassistUtil().getNestedClasses( classes, ctClass );
        }

        return classes.toArray( new CtClass[0] );
    }
}

