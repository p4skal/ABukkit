package de.p4skal.abukkit.utils;

import de.p4skal.abukkit.ABukkit;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;

import java.lang.reflect.Field;

/**
 * Util for Bukkit / CraftBukkit Stuff.
 *
 * @author p4skal
 */
@Getter
@RequiredArgsConstructor
public class CraftBukkitUtil {
    private final ABukkit aBukkit;

    /**
     * Get CommandMap instance from CraftBukkit using Reflection.
     *
     * @return Bukkit Command Map.
     * @throws NoSuchFieldException   When Java can't find the CommandMap inside the Server implementing Class.
     * @throws IllegalAccessException When Java can't access the value of the Field.
     */
    public CommandMap getCommandMap( ) throws NoSuchFieldException, IllegalAccessException {
        // Get declared Field for Command Map from CraftServer.
        Field field = Bukkit.getServer().getClass().getDeclaredField( "commandMap" );
        // Make Field accessible.
        field.setAccessible( true );

        // Get and return Command Map.
        return (CommandMap) field.get( Bukkit.getServer() );
    }
}
