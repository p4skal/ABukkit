package de.p4skal.abukkit.annotations;

import de.p4skal.abukkit.annotations.utils.Argument;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Use this Annotation to listen for a {@link org.bukkit.command.Command}.
 * Only works with @Inject or when Method is static.
 *
 * @author p4skal
 */
@Target( ElementType.METHOD )
@Retention( RetentionPolicy.RUNTIME )
public @interface Command {
    /**
     * Name(s) of the Command.
     *
     * @return Names as String Array.
     */
    String[] name( );

    /**
     * Permissions need to run this Command.
     *
     * @return Permissions as String Array.
     */
    String[] permissions( ) default { };

    /**
     * Arguments of this Command.
     *
     * @return Arguments as Argument Class Array.
     */
    Class<? extends Argument>[] arguments( ) default { };

    /**
     * Description of the Command.
     *
     * @return Description as String.
     */
    String description( ) default "";

    /**
     * Usage of the Command.
     *
     * @return Usage as String.
     */
    String usage( ) default "/";
}
