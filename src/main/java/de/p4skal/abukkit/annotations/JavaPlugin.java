package de.p4skal.abukkit.annotations;

import de.p4skal.abukkit.inspectors.pre.PluginInspector;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Use this Annotation to initialize a ABukkit Plugin.
 * Simplify {@code extends JavaPlugin}. Only once per Plugin!
 *
 * @author p4skal
 * @see PluginInspector
 */
@Target( ElementType.TYPE )
@Retention( RetentionPolicy.RUNTIME )
public @interface JavaPlugin {
    /**
     * Name of the Plugin.
     *
     * @return Plugin-Name as String.
     */
    String name( );

    /**
     * Name(s) of Author(s).
     *
     * @return Name(s) as String Array.
     */
    String[] authors( ) default { };

    /**
     * Version of the Plugin.
     *
     * @return Version as String.
     */
    String version( ) default "1.0.0-SNAPSHOT";
}
