package de.p4skal.abukkit.annotations.utils;

import lombok.Getter;

import java.util.regex.Pattern;

/**
 * @author p4skal
 */
@Getter
public class StringArgument extends Argument {
    private final Pattern pattern = Pattern.compile( "^[\\S]*$" );
    private final String standard = null;
    private final Class<?>[] types = new Class[]{ String.class };
}
