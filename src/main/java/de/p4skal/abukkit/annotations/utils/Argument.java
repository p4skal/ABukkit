package de.p4skal.abukkit.annotations.utils;

import java.util.regex.Pattern;

/**
 * @author p4skal
 */
public abstract class Argument {
    /**
     * The Pattern the Argument should match.
     *
     * @return Regex expression compiled to Pattern.
     */
    public abstract Pattern getPattern( );

    /**
     * Default value of Argument.
     *
     * @return Default Value as String.
     */
    public abstract String getStandard( );

    /**
     * Types that could be indicated with this Argument.
     *
     * @return Types as Class Array.
     */
    public abstract Class<?>[] getTypes( );

    /**
     * Check if any indicated Type is equal, or Superclass of given Class.
     *
     * @param lowerClass To check for.
     * @return True if any Type is equal, or Superclass.
     */
    public boolean isAssignableFrom( Class<?> lowerClass ) {
        // Iterate Types.
        for ( Class<?> aClass : this.getTypes() ) {
            // Check if aClass is superclass or equal to lowerClass.
            if ( aClass.isAssignableFrom( lowerClass ) ) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if any indicated Type is equal, or Lowerclass of given Class.
     *
     * @param superClass To check for.
     * @return True if any Type is equal, or Lowerclass.
     */
    public boolean isAssignableTo( Class<?> superClass ) {
        // Iterate Types.
        for ( Class<?> aClass : this.getTypes() ) {
            // Check if aClass is lowerClass or equal to superClass.
            if ( superClass.isAssignableFrom( aClass ) ) {
                return true;
            }
        }
        return false;
    }

}
