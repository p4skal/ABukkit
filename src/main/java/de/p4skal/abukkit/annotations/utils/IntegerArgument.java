package de.p4skal.abukkit.annotations.utils;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.regex.Pattern;

/**
 * @author p4skal
 */
@Getter
@NoArgsConstructor
public class IntegerArgument extends Argument {
    private final Pattern pattern = Pattern.compile( "^[1-9][\\d]{0,10}$" );
    private final String standard = null;
    private final Class<?>[] types = new Class[]{ Integer.class, int.class };
}
