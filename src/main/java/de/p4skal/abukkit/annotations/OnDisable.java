package de.p4skal.abukkit.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Use this Annotation to Listen when Server changes to disable state.
 *
 * @author p4skal
 */
@Target( ElementType.METHOD )
@Retention( RetentionPolicy.RUNTIME )
public @interface OnDisable {
    /**
     * Priority to order which Listener should get called first.
     * 0 first, .. last.
     */
    int priority( ) default 5;
}

