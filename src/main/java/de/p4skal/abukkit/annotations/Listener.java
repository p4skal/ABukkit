package de.p4skal.abukkit.annotations;

import de.p4skal.abukkit.inspectors.post.ListenerInspector;
import org.bukkit.event.Event;
import org.bukkit.event.EventPriority;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Use this Annotation to listen for a specified {@link org.bukkit.event.Event}.
 * Only works with @Inject or when Method is static.
 *
 * @author p4skal
 * @see ListenerInspector
 */
@Target( ElementType.METHOD )
@Retention( RetentionPolicy.RUNTIME )
public @interface Listener {
    /**
     * Event to Listen for.
     *
     * @return Event Type as Class.
     */
    Class<? extends Event> event();

    /**
     * The priority of the Event.
     *
     * @return Priority as EventPriority.
     */
    EventPriority priority() default EventPriority.NORMAL;
}
