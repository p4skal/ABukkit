package de.p4skal.abukkit.bukkit.listeners;

import de.p4skal.abukkit.ABukkit;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import java.lang.reflect.InvocationTargetException;

/**
 * Listen for all Type of Events, to fire all {@link de.p4skal.abukkit.entities.AListener}'s.
 * Interface between ABukkit and Bukkit.
 *
 * @author p4skal
 */
@Getter
@RequiredArgsConstructor
public class EventListener implements Listener {
    private final ABukkit aBukkit;

    /**
     * Listen for all incoming Events. listen on lowest Priority.
     *
     * @param event The Event instance.
     */
    @EventHandler( priority = EventPriority.LOWEST )
    public void onEventLowest( Event event ) {
        try {
            this.getABukkit().getBukkitHandler().invokeEvent( event, EventPriority.LOWEST );
        } catch ( InvocationTargetException | IllegalAccessException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Listen for all incoming Events. listen on low Priority.
     *
     * @param event The Event instance.
     */
    @EventHandler( priority = EventPriority.LOW )
    public void onEventLow( Event event ) {
        try {
            this.getABukkit().getBukkitHandler().invokeEvent( event, EventPriority.LOW );
        } catch ( InvocationTargetException | IllegalAccessException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Listen for all incoming Events. listen on normal Priority.
     *
     * @param event The Event instance.
     */
    @EventHandler( priority = EventPriority.NORMAL )
    public void onEventNormal( Event event ) {
        try {
            this.getABukkit().getBukkitHandler().invokeEvent( event, EventPriority.NORMAL );
        } catch ( InvocationTargetException | IllegalAccessException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Listen for all incoming Events. listen on high Priority.
     *
     * @param event The Event instance.
     */
    @EventHandler( priority = EventPriority.HIGH )
    public void onEventHigh( Event event ) {
        try {
            this.getABukkit().getBukkitHandler().invokeEvent( event, EventPriority.HIGH );
        } catch ( InvocationTargetException | IllegalAccessException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Listen for all incoming Events. listen on highest Priority.
     *
     * @param event The Event instance.
     */
    @EventHandler( priority = EventPriority.HIGHEST )
    public void onEventHighest( Event event ) {
        try {
            this.getABukkit().getBukkitHandler().invokeEvent( event, EventPriority.HIGHEST );
        } catch ( InvocationTargetException | IllegalAccessException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Listen for all incoming Events. listen on monitor Priority.
     *
     * @param event The Event instance.
     */
    @EventHandler( priority = EventPriority.MONITOR )
    public void onEventMonitor( Event event ) {
        try {
            this.getABukkit().getBukkitHandler().invokeEvent( event, EventPriority.MONITOR );
        } catch ( InvocationTargetException | IllegalAccessException e ) {
            e.printStackTrace();
        }
    }
}
