package de.p4skal.abukkit.bukkit.commands;

import com.google.common.collect.Lists;
import de.p4skal.abukkit.entities.ACommand;
import gnu.trove.set.hash.THashSet;
import lombok.AccessLevel;
import lombok.Getter;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.lang.reflect.InvocationTargetException;
import java.util.Set;

/**
 * Represent a BukkitCommand. Interface between ABukkit and Bukkit.
 *
 * @author p4skal
 */
@Getter( AccessLevel.PRIVATE )
public class BukkitCommand extends Command {
    /**
     * Contain all Command that have the same Name.
     *
     * @see #getName()
     */
    private Set<ACommand> commandPool = new THashSet<>();

    /**
     * Constructor of BukkitCommand.
     *
     * @param name    The Name of the Command.
     * @param command The ACommand.
     */
    public BukkitCommand( String name, ACommand command ) {
        super( name, command.getDescription(), command.getUsage(), Lists.newArrayList() );
    }

    /**
     * Add ACommand.
     *
     * @param aCommand The ACommand to add.
     */
    public void addACommand( ACommand aCommand ) {
        // Check for Description.
        if ( this.getDescription().isEmpty() && !aCommand.getDescription().isEmpty() ) {
            // Inherit Description of current ACommand.
            this.description = aCommand.getDescription();
        }

        // Check for Usage.
        if ( this.getUsage().equals( "/" ) && !aCommand.getUsage().equals( "/" ) ) {
            // Inherit Usage of current ACommand.
            this.usageMessage = aCommand.getUsage();
        }

        // Add ACommand to local Command Pool.
        this.getCommandPool().add( aCommand );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean execute( CommandSender sender, String label, String[] args ) {
        // Define Variable to store Permission Status.
        boolean noPerm = true;

        // Iterate local Command pool.
        for ( ACommand aCommand : this.getCommandPool() ) {
            // Define Variable to store Permission Status of current ACommand.
            boolean commandNoPerm = true;

            // Iterate Permissions.
            for ( String permission : aCommand.getPermissions() ) {
                // Check if Sender has Permission.
                if ( sender.hasPermission( permission ) ) {
                    // Change local Permission Status.
                    commandNoPerm = false;
                    break;
                }
            }

            // Check if Sender has no Permission for current Command.
            if ( commandNoPerm ) {
                continue;
            }

            // Modify Permission Status.
            noPerm = false;

            try {
                // Invoke Command.
                aCommand.invoke( sender, args );
            } catch ( InvocationTargetException | IllegalAccessException e ) {
                e.printStackTrace();
            }
        }

        // Check if sender had no access to any ACommand.
        if ( noPerm ) {
            // Send No-Permission Message.
            sender.sendMessage( this.getPermissionMessage() );
        }

        return true;
    }
}
