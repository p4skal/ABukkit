package de.p4skal.abukkit.inspectors;

import de.p4skal.abukkit.entities.utils.APriority;
import javassist.ClassPool;
import javassist.CtClass;

import java.io.File;

/**
 * TODO
 *
 * @author p4skal
 */
public interface PreInspector extends APriority {
    /**
     * Inspect Javassist Class. E.g. find MainClass.
     *
     * @param plugin    File of the APlugin.
     * @param classPool The Javassist ClassPool.
     * @param ctClasses Classes of Plugin.
     * @throws ClassNotFoundException Called when Javassist can't find Class in Jar File.
     */
    void inspect( File plugin, ClassPool classPool, CtClass[] ctClasses ) throws ClassNotFoundException;
}
