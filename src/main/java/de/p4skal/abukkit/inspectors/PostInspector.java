package de.p4skal.abukkit.inspectors;

import de.p4skal.abukkit.entities.APlugin;
import de.p4skal.abukkit.entities.utils.APriority;
import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;

/**
 * Implement this Interface to listen for a Class inspection.
 *
 * @author p4skal
 */
public interface PostInspector extends APriority {
    /**
     * Inspect Javassist Class. E.g. find Annotations.
     *
     * @param plugin    The Plugin.
     * @param classPool The Javassist ClassPool.
     * @param ctClasses The Classes to inspect.
     */
    void inspect( APlugin plugin, ClassPool classPool, CtClass[] ctClasses ) throws ClassNotFoundException,
            NotFoundException, CannotCompileException, NoSuchMethodException, IllegalAccessException,
            InstantiationException, NoSuchFieldException;
}
