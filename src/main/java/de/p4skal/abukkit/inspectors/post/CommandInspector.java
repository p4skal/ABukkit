package de.p4skal.abukkit.inspectors.post;

import de.p4skal.abukkit.ABukkit;
import de.p4skal.abukkit.annotations.Command;
import de.p4skal.abukkit.annotations.utils.Argument;
import de.p4skal.abukkit.entities.ACommand;
import de.p4skal.abukkit.entities.APlugin;
import de.p4skal.abukkit.inspectors.PostInspector;
import javassist.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.lang.reflect.Method;

/**
 * Command inspector. Listening for Command Annotation to register Command.
 *
 * @author p4skal
 */
@Getter
@RequiredArgsConstructor
public class CommandInspector implements PostInspector {
    private final ABukkit aBukkit;

    /**
     * {@inheritDoc}
     */
    @Override
    public void inspect( APlugin plugin, ClassPool classPool, CtClass[] ctClasses ) throws ClassNotFoundException,
            NotFoundException, CannotCompileException, NoSuchMethodException, IllegalAccessException, InstantiationException, NoSuchFieldException {
        // Iterate Classes.
        for ( CtClass ctClass : ctClasses ) {
            // Iterate Methods in Class.
            for ( CtMethod ctMethod : ctClass.getMethods() ) {
                // Check for JavaPlugin Annotation.
                if ( ctMethod.hasAnnotation( Command.class ) ) {
                    // Get and cast Command Annotation.
                    Command command = (Command) ctMethod.getAnnotation( Command.class );

                    // Transform Javassist Class to Java Class.
                    Class<?> clazz = ctClass.toClass();

                    // Find Java Method.
                    Method method = this.getABukkit().getJavassistUtil().getJavaMethod( ctClass, ctMethod );

                    // Define Argument Array.
                    Argument[] arguments = new Argument[command.arguments().length];

                    // Iterate Argument Classes.
                    for ( int i = 0; i < command.arguments().length; i++ ) {
                        // Create new Argument instance and add to Argument Array.
                        arguments[i] = command.arguments()[i].newInstance();
                    }

                    // Create ACommand instance.
                    ACommand aCommand = new ACommand( this.getABukkit(), clazz, method, plugin, command.name(),
                            command.permissions(), arguments, command.description(), command.usage() );

                    // Add Command.
                    this.getABukkit().getBukkitHandler().addCommand( aCommand );
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPriority( ) {
        return 0;
    }
}
