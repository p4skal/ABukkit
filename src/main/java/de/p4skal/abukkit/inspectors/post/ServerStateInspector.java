package de.p4skal.abukkit.inspectors.post;

import de.p4skal.abukkit.ABukkit;
import de.p4skal.abukkit.annotations.OnDisable;
import de.p4skal.abukkit.annotations.OnEnable;
import de.p4skal.abukkit.annotations.OnLoad;
import de.p4skal.abukkit.entities.APlugin;
import de.p4skal.abukkit.entities.AServerState;
import de.p4skal.abukkit.entities.utils.ServerState;
import de.p4skal.abukkit.inspectors.PostInspector;
import javassist.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.lang.reflect.Method;

/**
 * Server State inspector. Listening for OnLoad, OnEnable, OnDisable Annotation to register ServerStateListener.
 *
 * @author p4skal
 */
@Getter
@RequiredArgsConstructor
public class ServerStateInspector implements PostInspector {
    private final ABukkit aBukkit;

    /**
     * {@inheritDoc}
     */
    @Override
    public void inspect( APlugin plugin, ClassPool classPool, CtClass[] ctClasses ) throws ClassNotFoundException,
            NotFoundException, CannotCompileException, NoSuchMethodException {
        // Iterate Classes.
        for ( CtClass ctClass : ctClasses ) {
            // Iterate Methods in Class.
            for ( CtMethod ctMethod : ctClass.getMethods() ) {
                // Check for Server State Annotation.
                if ( !ctMethod.hasAnnotation( OnLoad.class ) || !ctMethod.hasAnnotation( OnEnable.class ) ||
                        !ctMethod.hasAnnotation( OnDisable.class ) ) {
                    continue;
                }

                // Declare Server State.
                ServerState serverState;

                // Declare Priority.
                int priority;

                // Transform Javassist Class to Java Class.
                Class<?> clazz = ctClass.toClass();

                // Find Java Method.
                Method method = this.getABukkit().getJavassistUtil().getJavaMethod( ctClass, ctMethod );

                // Check for JavaPlugin Annotation.
                if ( ctMethod.hasAnnotation( OnLoad.class ) ) {
                    // Get and cast OnLoad Annotation.
                    OnLoad onLoad = (OnLoad) ctMethod.getAnnotation( OnLoad.class );

                    // Fill ServerState.
                    serverState = ServerState.LOAD;

                    // Fill Priority.
                    priority = onLoad.priority();
                } else if ( ctMethod.hasAnnotation( OnEnable.class ) ) {
                    // Get and cast OnEnable Annotation.
                    OnEnable onEnable = (OnEnable) ctMethod.getAnnotation( OnEnable.class );

                    // Fill ServerState.
                    serverState = ServerState.ENABLE;

                    // Fill Priority.
                    priority = onEnable.priority();
                } else if ( ctMethod.hasAnnotation( OnDisable.class ) ) {
                    // Get and cast OnDisable Annotation.
                    OnDisable onDisable = (OnDisable) ctMethod.getAnnotation( OnDisable.class );

                    // Fill ServerState.
                    serverState = ServerState.DISABLE;

                    // Fill Priority.
                    priority = onDisable.priority();
                } else {
                    continue;
                }

                // Create ServerState Entity.
                AServerState aServerState = new AServerState( this.getABukkit(), clazz, method, serverState, priority );

                // Add to ServerState Pool.
                this.getABukkit().getBukkitHandler().getServerStatePool().put( serverState, aServerState );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPriority( ) {
        return 5;
    }
}
