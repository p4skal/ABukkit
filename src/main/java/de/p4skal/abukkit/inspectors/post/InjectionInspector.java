package de.p4skal.abukkit.inspectors.post;

import de.p4skal.abukkit.ABukkit;
import de.p4skal.abukkit.annotations.Inject;
import de.p4skal.abukkit.entities.AInjection;
import de.p4skal.abukkit.entities.APlugin;
import de.p4skal.abukkit.inspectors.PostInspector;
import javassist.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Injection inspector. Listening for Inject Annotation to create / inject Class.
 *
 * @author p4skal
 */
@Getter
@RequiredArgsConstructor
public class InjectionInspector implements PostInspector {
    private final ABukkit aBukkit;

    /**
     * {@inheritDoc}
     */
    @Override
    public void inspect( APlugin plugin, ClassPool classPool, CtClass[] ctClasses ) throws ClassNotFoundException,
            CannotCompileException {
        // Iterate Classes.
        for ( CtClass ctClass : ctClasses ) {
            // Check for Inject Annotation.
            if ( ctClass.hasAnnotation( Inject.class ) ) {
                // Get and cast Inject Annotation.
                Inject inject = (Inject) ctClass.getAnnotation( Inject.class );

                // Define Constructor.
                CtConstructor ctConstructor;

                try {
                    // Try to get empty Constructor.
                    ctConstructor = ctClass.getConstructor( "()V" );
                } catch ( NotFoundException e ) {
                    continue;
                }

                // Create Injection Entity.
                AInjection injection = new AInjection( inject.priority(), ctClass.toClass() );

                // Add Class to injection List.
                this.getABukkit().getInjectionHandler().getCreationPool().put( plugin, injection );
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPriority( ) {
        return 5;
    }
}
