package de.p4skal.abukkit.inspectors.post;

import de.p4skal.abukkit.ABukkit;
import de.p4skal.abukkit.annotations.BukkitPlugin;
import de.p4skal.abukkit.entities.ABukkitPlugin;
import de.p4skal.abukkit.entities.APlugin;
import de.p4skal.abukkit.inspectors.PostInspector;
import javassist.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.lang.reflect.Field;

/**
 * BukkitPlugin inspector. Listening for BukkitPlugin Annotation to fill Field with Bukkit Plugin.
 *
 * @author p4skal
 */
@Getter
@RequiredArgsConstructor
public class BukkitPluginInspector implements PostInspector {
    private final ABukkit aBukkit;

    /**
     * {@inheritDoc}
     */
    @Override
    public void inspect( APlugin plugin, ClassPool classPool, CtClass[] ctClasses ) throws ClassNotFoundException,
            CannotCompileException, NoSuchFieldException {
        // Iterate Classes.
        for ( CtClass ctClass : ctClasses ) {
            // Check for APlugin.
            if ( !APlugin.class.isAssignableFrom( ctClass.toClass() ) ) {
                continue;
            }

            // Iterate Fields.
            for ( CtField ctField : ctClass.getDeclaredFields() ) {
                if ( ctField.hasAnnotation( BukkitPlugin.class ) ) {
                    // Get and cast Annotation.
                    BukkitPlugin listener = (BukkitPlugin) ctField.getAnnotation( BukkitPlugin.class );

                    // Transform Javassist Class to Java Class.
                    Class<?> clazz = ctClass.toClass();

                    // Find Java Field.
                    Field field = this.getABukkit().getJavassistUtil().getJavaField( ctClass, ctField );

                    // Create and add BukkitPlugin Field to BukkitPlugin Pool.
                    this.getABukkit().getBukkitHandler().getBukkitPluginPool().add(
                            new ABukkitPlugin( this.getABukkit(), clazz, plugin, field )
                    );
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPriority( ) {
        return 0;
    }
}
