package de.p4skal.abukkit.inspectors.post;

import de.p4skal.abukkit.ABukkit;
import de.p4skal.abukkit.annotations.Listener;
import de.p4skal.abukkit.entities.AListener;
import de.p4skal.abukkit.entities.APlugin;
import de.p4skal.abukkit.inspectors.PostInspector;
import javassist.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.lang.reflect.Method;

/**
 * Listener inspector. Listening for Listener Annotation to register Listeners.
 *
 * @author p4skal
 */
@Getter
@RequiredArgsConstructor
public class ListenerInspector implements PostInspector {
    private final ABukkit aBukkit;

    /**
     * {@inheritDoc}
     */
    @Override
    public void inspect( APlugin plugin, ClassPool classPool, CtClass[] ctClasses ) throws ClassNotFoundException,
            NotFoundException, CannotCompileException, NoSuchMethodException {
        // Iterate Classes.
        for ( CtClass ctClass : ctClasses ) {
            // Iterate Methods in Class.
            for ( CtMethod ctMethod : ctClass.getMethods() ) {
                // Check for JavaPlugin Annotation.
                if ( ctMethod.hasAnnotation( Listener.class ) ) {
                    // Get and cast Annotation.
                    Listener listener = (Listener) ctMethod.getAnnotation( Listener.class );

                    // Transform Javassist Class to Java Class.
                    Class<?> clazz = ctClass.toClass();

                    // Find Java Method.
                    Method method = this.getABukkit().getJavassistUtil().getJavaMethod( ctClass, ctMethod );

                    // Create and add Listener to Listener Pool.
                    this.getABukkit().getBukkitHandler().getListenerPool().put(
                            listener.priority(),
                            new AListener( this.getABukkit(), listener.event(), listener.priority(), clazz, method )
                    );
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPriority( ) {
        return 5;
    }
}
