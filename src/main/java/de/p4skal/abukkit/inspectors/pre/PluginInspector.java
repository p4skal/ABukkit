package de.p4skal.abukkit.inspectors.pre;

import de.p4skal.abukkit.ABukkit;
import de.p4skal.abukkit.annotations.JavaPlugin;
import de.p4skal.abukkit.entities.APlugin;
import de.p4skal.abukkit.inspectors.PreInspector;
import javassist.ClassPool;
import javassist.CtClass;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.File;

/**
 * Plugin inspector. Listening for JavaPlugin Annotation to build up {@link APlugin}.
 * Only called on loading.
 *
 * @author p4skal
 */
@Getter
@RequiredArgsConstructor
public class PluginInspector implements PreInspector {
    private final ABukkit aBukkit;

    /**
     * {@inheritDoc}
     */
    public void inspect( File plugin, ClassPool classPool, CtClass[] ctClasses ) throws ClassNotFoundException {
        // Iterate Classes.
        for ( CtClass ctClass : ctClasses ) {
            // Check for JavaPlugin Annotation.
            if ( ctClass.hasAnnotation( JavaPlugin.class ) ) {
                // Get and cast Annotation.
                JavaPlugin data = (JavaPlugin) ctClass.getAnnotation( JavaPlugin.class );

                // Check if Reference Map contains Plugin.
                if ( this.getABukkit().getPluginHandler().getReferenceMap().containsKey( plugin ) ) {
                    // Forward APlugin in Reference Map.
                    this.getABukkit().getPluginHandler().getReferenceMap().get( plugin )
                            .set( new APlugin( plugin, data.name(), data.authors(), data.version() ) );
                    break;
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPriority( ) {
        return 10;
    }
}
