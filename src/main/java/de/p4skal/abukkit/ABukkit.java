package de.p4skal.abukkit;

import de.p4skal.abukkit.bukkit.listeners.EventListener;
import de.p4skal.abukkit.entities.utils.ServerState;
import de.p4skal.abukkit.handlers.BukkitHandler;
import de.p4skal.abukkit.handlers.InjectionHandler;
import de.p4skal.abukkit.handlers.PluginHandler;
import de.p4skal.abukkit.inspectors.PostInspector;
import de.p4skal.abukkit.inspectors.PreInspector;
import de.p4skal.abukkit.inspectors.post.CommandInspector;
import de.p4skal.abukkit.inspectors.post.InjectionInspector;
import de.p4skal.abukkit.inspectors.post.ListenerInspector;
import de.p4skal.abukkit.inspectors.post.ServerStateInspector;
import de.p4skal.abukkit.inspectors.pre.PluginInspector;
import de.p4skal.abukkit.utils.*;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.InvocationTargetException;

/**
 * Main Class of the ABukkit Plugin.
 *
 * TODO:
 * - check argument pattern
 * - create / add bukkit plugin per aplugin
 *
 * @author p4skal
 */
@Getter
public class ABukkit extends JavaPlugin {
    @Getter
    public static ABukkit aBukkit;

    /* --UTILS-- */

    private final JarUtil jarUtil = new JarUtil( this );
    private final PathUtil pathUtil = new PathUtil( this );
    private final JavassistUtil javassistUtil = new JavassistUtil();
    private final DIUtil diUtil = new DIUtil();
    private final CraftBukkitUtil craftBukkitUtil = new CraftBukkitUtil( this );

    /* --HANDLERS-- */

    private final PluginHandler pluginHandler = new PluginHandler( this );
    private final InjectionHandler injectionHandler = new InjectionHandler( this );
    private final BukkitHandler bukkitHandler = new BukkitHandler( this );

    /* --INSPECTORS-- */

    private final PluginInspector pluginInspector = new PluginInspector( this );
    private final InjectionInspector injectionInspector = new InjectionInspector( this );
    private final ListenerInspector listenerInspector = new ListenerInspector( this );
    private final ServerStateInspector serverStateInspector = new ServerStateInspector( this );
    private final CommandInspector commandInspector = new CommandInspector( this );

    /* --BUKKIT-- */

    private EventListener eventListener = new EventListener( this );

    /**
     * Constructor of ABukkit.
     */
    public ABukkit( ) {
        super();
        // Pass Main instance.
        ABukkit.aBukkit = this;

        // Load Libraries.
        this.getPluginHandler().loadLibraries();

        /* --INSPECTORS-- */

        // Register Injection Inspector.
        this.registerInspector( this.getInjectionInspector() );
        // Register Listener Inspector.
        this.registerInspector( this.getListenerInspector() );
        // Register ServerState Inspector.
        this.registerInspector( this.getServerStateInspector() );
        // Register Command Inspector.
        this.registerInspector( this.getCommandInspector() );

        // Load and inject each Plugin.
        this.getPluginHandler().loadPlugins();

        // DI for each Plugin.
        this.getInjectionHandler().injectPlugins();

        // Initialize Bukkit Plugin Fields, Listeners, Server State Listeners and Commands for each Plugin.
        this.getBukkitHandler().init();

        // TODO: init APlugins to fill Plugin Field!

        // Fill Bukkit Plugin annotated Fields.
        this.getBukkitHandler().fillBukkitPlugins();

        // Block Constructors of DI Classes.
        this.getPluginHandler().blockConstructors();
    }

    /**
     * Called when Bukkit loads every Plugin.
     */
    @Override
    public void onLoad( ) {
        // Register Bukkit Event Listener.
        this.getServer().getPluginManager().registerEvents( this.getEventListener(), this );

        try {
            // Try to invoke all Server State Listeners listening to Load State.
            this.getBukkitHandler().invokeServerState( ServerState.LOAD );
        } catch ( InvocationTargetException | IllegalAccessException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Called when Bukkit enables every Plugin.
     */
    @Override
    public void onEnable( ) {
        try {
            // Try to invoke all Server State Listeners listening to Load State.
            this.getBukkitHandler().invokeServerState( ServerState.ENABLE );
        } catch ( InvocationTargetException | IllegalAccessException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Called when Bukkit disables every Plugin.
     */
    @Override
    public void onDisable( ) {
        try {
            // Try to invoke all Server State Listeners listening to Load State.
            this.getBukkitHandler().invokeServerState( ServerState.LOAD );
        } catch ( InvocationTargetException | IllegalAccessException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Register Pre Inspector.
     *
     * @param preInspector To register.
     */
    public void registerInspector( PreInspector preInspector ) {
        this.getPluginHandler().getPreInspectors().add( preInspector );
    }

    /**
     * Register Post Inspector.
     *
     * @param postInspector To register.
     */
    public void registerInspector( PostInspector postInspector ) {
        this.getPluginHandler().getPostInspectors().add( postInspector );
    }
}
