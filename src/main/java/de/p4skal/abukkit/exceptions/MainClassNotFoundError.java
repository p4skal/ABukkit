package de.p4skal.abukkit.exceptions;

/**
 * Thrown when PluginInspector can't find a MainClass.
 *
 * @author p4skal
 */
public class MainClassNotFoundError extends Error {
    /**
     * Constructor of MainClassNotFoundError.
     */
    public MainClassNotFoundError( ) {
        super( "No MainClass found!" );
    }
}
