package de.p4skal.abukkit.exceptions;

import de.p4skal.abukkit.entities.AInjection;

/**
 * Thrown when Injection Process fails with nullable Instance.
 *
 * @author p4skal
 */
public class InjectionError extends Error {
    /**
     * Constructor of InjectionError.
     *
     * @param injection Injection where injecting failed.
     */
    public InjectionError( AInjection injection ) {
        super( "Injection failed!" + injection.toString() );
    }
}
