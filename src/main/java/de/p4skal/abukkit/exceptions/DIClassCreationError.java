package de.p4skal.abukkit.exceptions;

/**
 * Thrown when you try to create an Instance of a Inject Annotated Class.
 *
 * @author p4skal
 */
public class DIClassCreationError extends Error {
    /**
     * Constructor of DIClassCreationError.
     */
    public DIClassCreationError( ) {
        super( "Can't create Instance of a Inject Annotated Class." );
    }
}
