package de.p4skal.abukkit.exceptions;

import de.p4skal.abukkit.annotations.utils.Argument;

/**
 * Thrown when Command Argument has an incompatible Type to fit.
 *
 * @author p4skal
 */
public class ArgumentIsNotAssignableError extends Error {
    /**
     * Constructor of ArgumentIsNotAssignableError.
     *
     * @param argument That is incompatible with Parameter Type.
     */
    public ArgumentIsNotAssignableError( Argument argument ) {
        super( "Parameter Type is not assignable to Argument(" + argument.getClass() + ")!" );
    }
}