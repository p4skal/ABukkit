package de.p4skal.abukkit.entities;

import com.google.common.collect.Lists;
import de.p4skal.abukkit.ABukkit;
import de.p4skal.abukkit.entities.utils.AMethodEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.bukkit.event.Event;
import org.bukkit.event.EventPriority;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

/**
 * Represents a Listener annotated Method. Contains all the Data for the Listener.
 *
 * @author p4skal
 */
@Data
@EqualsAndHashCode( callSuper = true )
public class AListener extends AMethodEntity {
    /**
     * Event Type to listen for.
     */
    private final Class<? extends Event> event;
    /**
     * The priority of the Event.
     */
    private final EventPriority eventPriority;
    /**
     * The Positions to replace with Events in Cached Parameters Array.
     */
    private Integer[] eventPositions;

    /**
     * Constructor of AListener.
     *
     * @param aBukkit       The ABukkit Main Class instance.
     * @param event         The Event Type to listen for.
     * @param eventPriority The priority of the Event.
     * @param aClass        The Class contain the Method to invoke.
     * @param method        The Method to invoke.
     */
    public AListener( ABukkit aBukkit, Class<? extends Event> event, EventPriority eventPriority, Class<?> aClass,
                      Method method ) {
        super( aBukkit, aClass, method );
        this.event = event;
        this.eventPriority = eventPriority;
    }

    /**
     * Initialize the Listener. Called when DI is complete.
     */
    @Override
    public void postInit( ) {
        // Call super Method.
        super.postInit();

        // Define Event Position List.
        List<Integer> eventPositions = Lists.newArrayList();

        // Iterate Method Parameters.
        for ( int i = 0; i < this.getMethod().getParameterTypes().length; i++ ) {
            // Check if Parameter Type is Assignable of Event.
            if ( this.getMethod().getParameterTypes()[i].isAssignableFrom( this.getEvent() ) ) {
                // Add Parameter to Event Position List.
                eventPositions.add( i );
            }
        }

        // Fill Event Parameter Array.
        this.eventPositions = eventPositions.toArray( new Integer[0] );
    }

    /**
     * Invoke Listener.
     *
     * @param event The Event.
     * @throws InvocationTargetException If Method throw Exception.
     * @throws IllegalAccessException    No access to invoke Method.
     */
    public void invoke( Event event ) throws InvocationTargetException, IllegalAccessException {
        // Call super Method with created Parameters.
        super.invoke( this.buildParameters( event ) );
    }

    /**
     * Copy Cached Parameters Array and fill in the Event instance.
     *
     * @param event To fill in.
     * @return Filled Parameter Array.
     */
    private Object[] buildParameters( Event event ) {
        // Get copy of Cached Parameters.
        final Object[] parameters = Arrays.copyOf( this.getParameters(), this.getParameters().length );

        // Iterate Event Positions.
        for ( Integer position : this.getEventPositions() ) {
            // Set Event instance in copied Array.
            parameters[position] = event;
        }

        return parameters;
    }
}
