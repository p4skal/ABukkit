package de.p4skal.abukkit.entities;

import de.p4skal.abukkit.ABukkit;
import de.p4skal.abukkit.entities.utils.ADIEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.lang.reflect.Field;

/**
 * @author p4skal
 */
@EqualsAndHashCode( callSuper = true )
@Data
public class ABukkitPlugin extends ADIEntity {
    private final APlugin plugin;
    /**
     * Field to fill.
     */
    private final Field field;

    /**
     * @param aBukkit The ABukkit Main Class instance.
     * @param aClass  The Class contain the Field to set.
     * @param plugin  The APlugin of the BukkitPlugin Field.
     * @param field   The Field to fill.
     */
    public ABukkitPlugin( ABukkit aBukkit, Class<?> aClass, APlugin plugin, Field field ) {
        super( aBukkit, aClass );
        this.plugin = plugin;
        this.field = field;
    }

    /**
     * Fill Field with Plugin instance.
     *
     * @throws IllegalAccessException When Java can't access the Field.
     */
    public void fill( ) throws IllegalAccessException {
        this.getField().setAccessible( true );
        this.getField().set( this.getInstance(), this.getPlugin().getPlugin() );
    }
}
