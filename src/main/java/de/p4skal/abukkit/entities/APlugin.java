package de.p4skal.abukkit.entities;

import de.p4skal.abukkit.entities.utils.APostInit;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import sun.plugin2.main.server.Plugin;

import java.io.File;

/**
 * Represents a Plugin. Contains all the Data.
 *
 * @author p4skal
 */
@Data
@RequiredArgsConstructor
public class APlugin implements APostInit {
    /**
     * The Jar File of the Plugin.
     */
    private final File file;
    /**
     * The Name of the Plugin.
     */
    private final String name;
    /**
     * Authors of the Plugin.
     */
    private final String[] authors;
    /**
     * The Version of the Plugin.
     */
    private final String version;
    /**
     * The Bukkit Plugin instance.
     */
    private Plugin plugin;

    /**
     * {@inheritDoc}
     */
    @Override
    public void postInit( ) {
        // TODO: fill with bukkit plugin instance.
        this.plugin = null;
    }
}
