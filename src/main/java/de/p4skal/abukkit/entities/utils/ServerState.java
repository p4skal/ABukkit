package de.p4skal.abukkit.entities.utils;

import de.p4skal.abukkit.entities.AServerState;

/**
 * Represent Server State.
 *
 * @author p4skal
 * @see AServerState
 */
public enum ServerState {
    /**
     * When Bukkit loads all Plugins.
     */
    LOAD,
    /**
     * When Bukkit enables all Plugins.
     */
    ENABLE,
    /**
     * When Bukkit disables all Plugin.
     */
    DISABLE
}
