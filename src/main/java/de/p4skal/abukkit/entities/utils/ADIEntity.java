package de.p4skal.abukkit.entities.utils;

import de.p4skal.abukkit.ABukkit;
import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * Util to build up Entities for DI Annotations.
 *
 * @author p4skal
 */
@Data
@RequiredArgsConstructor
public abstract class ADIEntity implements APostInit {
    protected final ABukkit aBukkit;
    /**
     * Class containing the Method to invoke.
     */
    protected final Class<?> aClass;
    /**
     * The instance to invoke on.
     */
    protected Object instance;

    /**
     * Method to fill instance from DI.
     */
    @Override
    public void postInit( ) {
        // Fill instance to call.
        this.instance = this.getABukkit().getDiUtil().getInjectionPool().get( this.getAClass() );
    }
}
