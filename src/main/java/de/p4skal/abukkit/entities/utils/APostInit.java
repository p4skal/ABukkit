package de.p4skal.abukkit.entities.utils;

/**
 * Make some Entity "init-able".
 *
 * @author p4skal
 */
public interface APostInit {
    /**
     * Call to Post Init some Field.
     */
    void postInit( );
}
