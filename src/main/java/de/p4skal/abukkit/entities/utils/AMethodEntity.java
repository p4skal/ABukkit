package de.p4skal.abukkit.entities.utils;

import de.p4skal.abukkit.ABukkit;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Util to build up Entities for Method Annotations.
 *
 * @author p4skal
 */
@EqualsAndHashCode( callSuper = true )
@Data
public abstract class AMethodEntity extends ADIEntity {
    /**
     * The Method to invoke.
     */
    protected final Method method;
    /**
     * The cached Parameters of Method.
     *
     * @see #postInit()
     */
    protected Object[] parameters;

    /**
     * Constructor of AMethodEntity.
     *
     * @param aBukkit The ABukkit Main Class instance.
     * @param aClass  The Class contain the Method to invoke.
     * @param method  The Method to invoke.
     */
    public AMethodEntity( ABukkit aBukkit, Class<?> aClass, Method method ) {
        super( aBukkit, aClass );
        this.method = method;
    }

    /**
     * Method to fill instance / parameters from DI.
     */
    public void postInit( ) {
        // Call super Method to Fill instance to call.
        super.postInit();

        // Create Cached Parameters Array.
        this.parameters = new Object[this.getMethod().getParameterCount()];

        // Iterate Method Parameters.
        for ( int i = 0; i < this.getMethod().getParameterTypes().length; i++ ) {
            // Get instance from Injection pool and bind it to the Parameter Cache.
            this.parameters[i] = this.getABukkit().getDiUtil().getAssignableTo( this.getMethod().getParameterTypes()[i] );
        }
    }

    /**
     * Call Method with given Parameters.
     *
     * @throws InvocationTargetException If Method throws Exception.
     * @throws IllegalAccessException    No access to invoke Method.
     * @see #getParameters()
     */
    public void invoke( ) throws InvocationTargetException, IllegalAccessException {
        this.invoke( this.getParameters() );
    }

    /**
     * Call Method with specified Parameters.
     *
     * @param parameters To invoke Method.
     * @throws InvocationTargetException If Method throws Exception.
     * @throws IllegalAccessException    No access to invoke Method.
     */
    protected void invoke( Object[] parameters ) throws InvocationTargetException, IllegalAccessException {
        // Make Method accessible.
        this.getMethod().setAccessible( true );
        // Invoke Method.
        this.getMethod().invoke( this.getInstance(), parameters );
    }
}
