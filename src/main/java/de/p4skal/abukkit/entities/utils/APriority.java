package de.p4skal.abukkit.entities.utils;

/**
 * Make some AEntity "priority-ble".
 *
 * @author p4skal
 */
public interface APriority extends Comparable<APriority> {
    /**
     * The Priority of the implementation.
     *
     * @return the smaller the number, the higher the priority.
     */
    int getPriority( );

    /**
     * {@inheritDoc}
     */
    @Override
    default int compareTo( APriority other ) {
        if ( this.getPriority() < other.getPriority() ) {
            return -1;
        } else {
            return 1;
        }
    }
}

