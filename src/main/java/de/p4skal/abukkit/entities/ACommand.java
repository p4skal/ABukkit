package de.p4skal.abukkit.entities;

import de.p4skal.abukkit.ABukkit;
import de.p4skal.abukkit.annotations.utils.Argument;
import de.p4skal.abukkit.entities.utils.AMethodEntity;
import de.p4skal.abukkit.exceptions.ArgumentIsNotAssignableError;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * Represents a Command annotated Method. Contains all the Data for the Command.
 *
 * @author p4skal
 */
@EqualsAndHashCode( callSuper = true )
@Data
public class ACommand extends AMethodEntity {
    /**
     * The APlugin of this Command.
     */
    private final APlugin aPlugin;
    /**
     * All valid Labels for this Command.
     */
    private final String[] names;
    /**
     * The Permissions to use this Command.
     */
    private final String[] permissions;
    /**
     * The Arguments for this Command.
     */
    private final Argument[] arguments;
    /**
     * Description of this Command.
     */
    private final String description;
    /**
     * Usage of this Command.
     */
    private final String usage;
    /**
     * 0 if the no Player Args; 1 if CommandSender Args; 2 if Player Args.
     */
    private int playerArgs = 0;

    /**
     * Constructor of ACommand.
     *
     * @param aBukkit     The ABukkit Main Class instance.
     * @param aClass      The Class contain the Method to invoke.
     * @param method      The Method to invoke.
     * @param aPlugin     The APlugin of this Command.
     * @param names       All valid Labels for this Command.
     * @param permissions The Permissions to use this Command.
     * @param arguments   The Arguments for this Command.
     * @param description The Description of this Command.
     * @param usage       The Usage of this Command.
     */
    public ACommand( ABukkit aBukkit, Class<?> aClass, Method method, APlugin aPlugin, String[] names,
                     String[] permissions, Argument[] arguments, String description, String usage ) {
        super( aBukkit, aClass, method );
        this.aPlugin = aPlugin;
        this.names = names;
        this.permissions = permissions;
        this.arguments = arguments;
        this.description = description;
        this.usage = usage;

        // Iterate Command Names.
        for ( int i = 0; i < this.getNames().length; i++ ) {
            // Format Command Name.
            this.getNames()[i] = this.getNames()[i].toLowerCase().trim();
        }
    }

    /**
     * Initialize the Command. Called when DI is complete.
     */
    @Override
    public void postInit( ) {
        // Call super Method.
        super.postInit();

        // Get Method Parameters.
        Class<?>[] parameterTypes = this.getMethod().getParameterTypes();

        // Check if Method has Parameters.
        if ( parameterTypes.length == 0 ) {
            return;
        }

        // Check if 0. Argument is a CommandSender / Player Argument.
        if ( parameterTypes[0].isAssignableFrom( CommandSender.class ) ) {
            this.playerArgs = 1;
        } else if ( parameterTypes[0].isAssignableFrom( Player.class ) ) {
            this.playerArgs = 2;
        }

        // Check for 1 Parameter Method.
        if ( parameterTypes.length == 1 ) {
            return;
        }

        // Check if 0. Argument is a Player Argument.
        if ( this.getPlayerArgs() == 0 ) {
            // Iterate Parameter Types in relation with Arguments.
            for ( int i = 1; i < parameterTypes.length && i < this.getArguments().length; i++ ) {
                // Check for incompatible Parameter Type.
                if ( !this.getArguments()[i].isAssignableTo( parameterTypes[i] ) ) {
                    // Throw Argument assignable Error.
                    throw new ArgumentIsNotAssignableError( this.getArguments()[i] );
                }
            }
        } else {
            // Iterate Parameter Types in relation with Arguments.
            for ( int i = 1; i < parameterTypes.length && ( i - 1 ) < this.getArguments().length; i++ ) {
                // Check for incompatible Parameter Type.
                if ( !this.getArguments()[i - 1].isAssignableTo( parameterTypes[i] ) ) {
                    // Throw Argument assignable Error.
                    throw new ArgumentIsNotAssignableError( this.getArguments()[i - 1] );
                }
            }
        }
    }

    /**
     * Invoke Command.
     *
     * @param sender The Command Sender.
     * @param args   Given Arguments.
     * @throws InvocationTargetException If Method throw Exception.
     * @throws IllegalAccessException    No access to invoke Method.
     */
    public void invoke( CommandSender sender, Object[] args ) throws InvocationTargetException, IllegalAccessException {
        // Call super Method with created Parameters.
        super.invoke( this.buildParameters( sender, args ) );
    }

    /**
     * Copy Cached Parameters Array and fill in the Command Data.
     *
     * @param sender To fill Sender Parameter.
     * @param args   To fill Arguments.
     * @return Filled Parameter Array.
     */
    private Object[] buildParameters( CommandSender sender, Object[] args ) {
        // Get copy of Cached Parameters.
        final Object[] parameters = Arrays.copyOf( this.getParameters(), this.getParameters().length );

        // Check for Sender Argument.
        if ( this.getPlayerArgs() == 1 || this.getPlayerArgs() == 2 ) {
            // Fill Sender Argument.
            parameters[0] = sender;

            // Iterate Parameters.
            for ( int i = 1; i < parameters.length; i++ ) {
                // Check if Parameter count is longer than Sender Argument count.
                if ( args.length >= i ) {
                    // Check if Parameter count is longer than Annotation Argument count.
                    if ( this.getArguments().length >= i ) {
                        break;
                    }
                    // Fill Parameter with standard value.
                    parameters[i] = this.getArguments()[i - 1].getStandard();
                    continue;
                }
                try {
                    // Try to cast Sender Argument to Annotation Argument Type.
                    parameters[i] = this.getMethod().getParameterTypes()[i].getClass().cast( args[i - 1] );
                } catch ( ClassCastException ignored ) {
                    // Fill Parameter with standard value.
                    parameters[i] = this.getArguments()[i - 1];
                }
            }
        } else {
            for ( int i = 0; i < parameters.length; i++ ) {
                // Check if Parameter count is longer than Sender Argument count.
                if ( args.length >= ( i + 1 ) ) {
                    // Check if Parameter count is longer than Annotation Argument count.
                    if ( this.getArguments().length >= ( i + 1 ) ) {
                        break;
                    }
                    // Fill Parameter with standard value.
                    parameters[i] = this.getArguments()[i].getStandard();
                    continue;
                }
                try {
                    // Try to cast Sender Argument to Annotation Argument Type.
                    parameters[i] = this.getMethod().getParameterTypes()[i].getClass().cast( args[i] );
                } catch ( ClassCastException ignored ) {
                    // Fill Parameter with standard value.
                    parameters[i] = this.getArguments()[i];
                }
            }
        }

        return parameters;
    }
}
