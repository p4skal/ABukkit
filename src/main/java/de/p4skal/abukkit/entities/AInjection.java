package de.p4skal.abukkit.entities;

import de.p4skal.abukkit.entities.utils.APriority;
import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * Represent Class to Inject.
 *
 * @author p4skal
 */
@Data
@RequiredArgsConstructor
public class AInjection implements APriority {
    /**
     * Creation / Injection Priority. Time when Class should be created / injected.
     */
    private final int priority;
    /**
     * The Class to create / inject.
     */
    private final Class<?> aClass;
    /**
     * The created instance.
     */
    private Object instance = null;
}
