package de.p4skal.abukkit.entities;

import de.p4skal.abukkit.ABukkit;
import de.p4skal.abukkit.entities.utils.AMethodEntity;
import de.p4skal.abukkit.entities.utils.APriority;
import de.p4skal.abukkit.entities.utils.ServerState;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.lang.reflect.Method;

/**
 * Represents a OnLoad, OnEnable or OnDisable annotated Method. Contains all the Data for the Server State Listener.
 *
 * @author p4skal
 */
@Data
@EqualsAndHashCode( callSuper = true )
public class AServerState extends AMethodEntity implements APriority {
    /**
     * The State the Method listens to.
     */
    private final ServerState serverState;
    /**
     * The priority of the Listener.
     */
    private final int priority;

    /**
     * Constructor of AServerState.
     *
     * @param aBukkit     The ABukkit Main Class instance.
     * @param aClass      The Class contain the Method to invoke.
     * @param method      The Method to invoke.
     * @param serverState The State the Method listens to.
     * @param priority    The priority of the Listener.
     */
    public AServerState( ABukkit aBukkit, Class<?> aClass, Method method, ServerState serverState, int priority ) {
        super( aBukkit, aClass, method );
        this.serverState = serverState;
        this.priority = priority;
    }
}
