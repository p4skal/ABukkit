package de.p4skal.abukkit.handlers;

import com.google.common.collect.Sets;
import de.p4skal.abukkit.ABukkit;
import de.p4skal.abukkit.annotations.Inject;
import de.p4skal.abukkit.entities.APlugin;
import de.p4skal.abukkit.exceptions.MainClassNotFoundError;
import de.p4skal.abukkit.inspectors.PostInspector;
import de.p4skal.abukkit.inspectors.PreInspector;
import gnu.trove.map.TMap;
import gnu.trove.map.hash.THashMap;
import gnu.trove.set.hash.THashSet;
import javassist.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Handler load and parse APlugin.
 *
 * @author p4skal
 */
@AllArgsConstructor
@Getter
public class PluginHandler {
    private final ABukkit aBukkit;

    /**
     * Inspectors called when loading the Class.
     *
     * @see de.p4skal.abukkit.ABukkit#registerInspector(PreInspector)
     */
    private final Set<PreInspector> preInspectors = Sets.newTreeSet();
    /**
     * Inspectors called when inspecting the Class.
     *
     * @see de.p4skal.abukkit.ABukkit#registerInspector(PostInspector)
     */
    private final Set<PostInspector> postInspectors = Sets.newTreeSet();

    /**
     * Contains File to Plugin mapping.
     */
    private final TMap<File, AtomicReference<APlugin>> referenceMap = new THashMap<>();
    /**
     * Contain all Classes.
     */
    private final Set<CtClass> classPool = new THashSet<>();

    /**
     * Contains every APlugin instance.
     *
     * @see #loadPlugins()
     */
    private final Set<APlugin> pluginPool = new THashSet<>();

    /**
     * Load all Libraries.
     */
    public void loadLibraries( ) {
        try {
            // Iterate Libraries.
            for ( File file : this.getABukkit().getPathUtil().getLibraries() ) {
                // Load Library.
                this.loadLibrary( file );
            }
        } catch ( IOException | NotFoundException | CannotCompileException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Load and inspect all Plugins.
     */
    public void loadPlugins( ) {
        try {
            // Iterate Plugin Files.
            for ( File file : this.getABukkit().getPathUtil().getRawPlugins() ) {
                // Load and inspect every Plugin. Also add created instance to Plugin Pool.
                this.getPluginPool().add( this.loadPlugin( file ) );
            }
        } catch ( NotFoundException | IOException | CannotCompileException | ClassNotFoundException |
                NoSuchMethodException | InstantiationException | IllegalAccessException | NoSuchFieldException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Load Library.
     *
     * @param file The Library to load.
     * @throws NotFoundException      Called when Javassist can't find Library File.
     * @throws IOException            Called when Library File is no Jar File.
     * @throws CannotCompileException Called when Javassist can't transform CtClass to Class.
     */
    private void loadLibrary( File file ) throws NotFoundException, IOException, CannotCompileException {
        // Get ClassPool.
        ClassPool classPool = ClassPool.getDefault();
        // Add Jar to Pool.
        classPool.insertClassPath( file.getAbsolutePath() );

        // Define Set containing all Classes in Jar.
        CtClass[] ctClasses = this.getABukkit().getJarUtil().getClasses( classPool, file );

        // Iterate CtClasses.
        for ( CtClass ctClass : ctClasses ) {
            // Load Class in ClassPath.
            ctClass.toClass();
        }
    }

    /**
     * Load and inspect Plugin. Map to APlugin. Use @JavaPlugin to get Plugin Data.
     *
     * @param file File of the APlugin.
     * @return ABukkit Plugin. {@code null} if no @JavaPlugin Annotation exist.
     * @throws NotFoundException      Called when Javassist can't find Plugin File.
     * @throws IOException            Called when Plugin File is no Jar File.
     * @throws ClassNotFoundException Called when Javassist can't find Class in Jar File.
     * @throws CannotCompileException Called when Javassist can't transform CtClass to Class.
     * @throws NoSuchFieldException   Called when Java can't find Method.
     * @throws InstantiationException Called when Java can't create Instance of Class.
     * @throws IllegalAccessException Called when Java has no access to reflect something.
     * @throws NoSuchFieldException   Called when Java can't find Field.
     */
    private APlugin loadPlugin( File file ) throws NotFoundException, IOException, ClassNotFoundException,
            CannotCompileException, NoSuchMethodException, InstantiationException, IllegalAccessException,
            NoSuchFieldException {
        // Get ClassPool.
        ClassPool classPool = ClassPool.getDefault();
        // Add Jar to Pool.
        classPool.insertClassPath( file.getAbsolutePath() );

        // Define Set containing all Classes in Jar.
        CtClass[] ctClasses = this.getABukkit().getJarUtil().getClasses( classPool, file );

        // Iterate CtClasses.
        for ( CtClass ctClass : ctClasses ) {
            // Load Class in ClassPath.
            ctClass.toClass();
        }

        // Create Atomic Reference for APlugin.
        AtomicReference<APlugin> pluginReference = new AtomicReference<>( null );

        // Add mapping to Reference Map.
        this.getReferenceMap().put( file, pluginReference );

        // Iterate Pre Inspectors.
        for ( PreInspector preInspector : this.getPreInspectors() ) {
            // Inspect File.
            preInspector.inspect( file, classPool, ctClasses );
        }

        // Get Plugin.
        APlugin plugin = pluginReference.get();

        // Check if no MainClass was found.
        if ( plugin == null ) {
            throw new MainClassNotFoundError();
        }

        // Iterate Post Inspectors.
        for ( PostInspector postInspector : this.getPostInspectors() ) {
            // Inspect Plugin.
            postInspector.inspect( plugin, classPool, ctClasses );
        }

        // Add to Class Pool.
        Collections.addAll( this.getClassPool(), ctClasses );

        // Return Plugin.
        return plugin;
    }

    /**
     * Add {@link de.p4skal.abukkit.exceptions.DIClassCreationError} to each Constructor of every Injection Class.
     */
    public void blockConstructors( ) {
        try {
            // Iterate all Classes
            for ( CtClass ctClass : this.getClassPool() ) {
                // Check for Inject Annotation.
                if ( !ctClass.hasAnnotation( Inject.class ) ) {
                    continue;
                }

                // Iterate Constructors.
                for ( CtConstructor ctConstructor : ctClass.getDeclaredConstructors() ) {
                    // Insert Error into the Constructor Body.
                    ctConstructor.insertAfter( "throw new de.p4skal.abukkit.exceptions.DIClassCreationError();" );
                }
                // Write changes to Class.
                ctClass.toClass();
            }
        } catch ( CannotCompileException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Get BukkitPlugin from APlugin.
     *
     * @param aPlugin To get Bukkit Plugin from.
     * @return Bukkit Plugin.
     */
    public Plugin provideBukkitPlugin( APlugin aPlugin ) {
        return this.getABukkit();
    }
}
