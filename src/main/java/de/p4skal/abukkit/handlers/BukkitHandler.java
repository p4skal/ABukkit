package de.p4skal.abukkit.handlers;

import com.google.common.collect.*;
import de.p4skal.abukkit.ABukkit;
import de.p4skal.abukkit.bukkit.commands.BukkitCommand;
import de.p4skal.abukkit.entities.*;
import de.p4skal.abukkit.entities.utils.ServerState;
import gnu.trove.set.hash.THashSet;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.event.Event;
import org.bukkit.event.EventPriority;

import java.lang.reflect.InvocationTargetException;
import java.util.Set;

/**
 * Handler contain all Bukkit Methods. E.g. Listeners etc.
 *
 * @author p4skal
 */
@Getter
@RequiredArgsConstructor
public class BukkitHandler {
    private final ABukkit aBukkit;
    /**
     * Contain all Fields that need to be filled with the current Bukkit Plugin.
     */
    private final Set<ABukkitPlugin> bukkitPluginPool = new THashSet<>();
    /**
     * Contain all Listeners to fire.
     */
    private final Multimap<EventPriority, AListener> listenerPool = MultimapBuilder.treeKeys().hashSetValues().build();
    /**
     * Contain all Server State listeners to fire.
     */
    private final Multimap<ServerState, AServerState> serverStatePool = MultimapBuilder.hashKeys().treeSetValues().build();
    /**
     * Contain all Commands to init.
     */
    private final Set<ACommand> commandPool = new THashSet<>();
    /**
     * Table to store created BukkitCommand instances.
     */
    private final Table<APlugin, String, BukkitCommand> registeredCommands = HashBasedTable.create();

    /**
     * Init all Listeners and Server State Listeners.
     */
    public void init( ) {
        // Iterate BukkitPlugin Fields.
        for ( ABukkitPlugin aBukkitPlugin : this.getBukkitPluginPool() ) {
            // Init Bukkit Plugin Field.
            aBukkitPlugin.postInit();
        }

        // Iterate Listeners.
        for ( AListener listener : this.getListenerPool().values() ) {
            // Init Listener.
            listener.postInit();
        }

        // Iterate Server State Listeners.
        for ( AServerState listener : this.getServerStatePool().values() ) {
            // Init Server State Listener.
            listener.postInit();
        }

        // Iterate all Commands.
        for ( ACommand aCommand : this.getCommandPool() ) {
            // Init Command.
            aCommand.postInit();
        }
    }

    /**
     * Fill all Bukkit Plugin Fields with Bukkit Plugin instances.
     */
    public void fillBukkitPlugins( ) {
        try {
            // Iterate BukkitPlugin Fields.
            for ( ABukkitPlugin aBukkitPlugin : this.getBukkitPluginPool() ) {
                // Fill Bukkit Plugin Field.
                aBukkitPlugin.fill();
            }
        } catch ( IllegalAccessException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Add Command to Command Pool. Register Command.
     *
     * @param aCommand The Command to register.
     * @throws NoSuchFieldException   When there is no CommandMap.
     * @throws IllegalAccessException When CommandMap is not accessible.
     */
    public void addCommand( ACommand aCommand ) throws NoSuchFieldException, IllegalAccessException {
        // Add Command to Command Pool.
        this.getCommandPool().add( aCommand );

        // Iterate Command Names.
        for ( String name : aCommand.getNames() ) {
            // Get BukkitCommand from registered Commands Table.
            BukkitCommand bukkitCommand = this.getRegisteredCommands().get( aCommand.getAPlugin(), name );

            // Check if BukkitCommand don't exist.
            if ( bukkitCommand == null ) {
                // Create Bukkit Command.
                bukkitCommand = new BukkitCommand( name, aCommand );

                // Add Command to registered Commands Table.
                this.getRegisteredCommands().put( aCommand.getAPlugin(), name, bukkitCommand );

                // Register Command internally.
                this.getABukkit().getCraftBukkitUtil().getCommandMap().register( name, bukkitCommand );
            } else {
                // Add ACommand to BukkitCommand with same Name.
                bukkitCommand.addACommand( aCommand );
            }
        }
    }

    /**
     * Fire all Listeners listen to this Event with given Priority.
     *
     * @param event To get Listeners and to fire.
     * @throws InvocationTargetException If the Event Method throws an Exception.
     * @throws IllegalAccessException    No access to invoke the Event Method.
     */
    public void invokeEvent( Event event, EventPriority eventPriority ) throws InvocationTargetException,
            IllegalAccessException {
        // Iterate Listeners with specified Priority.
        for ( AListener listener : this.getListenerPool().get( eventPriority ) ) {
            // Check if Class Type is equal or other Class Type inherits from this.
            if ( listener.getEvent().isAssignableFrom( event.getClass() ) ) {
                // Invoke Listener.
                listener.invoke( event );
            }
        }
    }

    /**
     * Fire all Server State Listeners listen to this State.
     *
     * @param serverState To get Listeners and to fire.
     * @throws InvocationTargetException If the Event Method throws an Exception.
     * @throws IllegalAccessException    No access to invoke the Event Method.
     */
    public void invokeServerState( ServerState serverState ) throws InvocationTargetException, IllegalAccessException {
        // Iterate Server State Listeners with specified State.
        for ( AServerState listener : this.getServerStatePool().get( serverState ) ) {
            // Invoke Server State Listener.
            listener.invoke();
        }
    }
}
