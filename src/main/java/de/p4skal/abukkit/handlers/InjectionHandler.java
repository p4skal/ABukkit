package de.p4skal.abukkit.handlers;

import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import de.p4skal.abukkit.ABukkit;
import de.p4skal.abukkit.annotations.Ignore;
import de.p4skal.abukkit.entities.AInjection;
import de.p4skal.abukkit.entities.APlugin;
import de.p4skal.abukkit.exceptions.InjectionError;
import de.p4skal.abukkit.utils.DIUtil;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.TreeSet;

/**
 * Handling Dependency Injection (DI) part.
 *
 * @author p4skal
 */
@Getter
@RequiredArgsConstructor
public class InjectionHandler {
    private final ABukkit aBukkit;
    /**
     * Contain Classes to create.
     */
    private Multimap<APlugin, AInjection> creationPool = MultimapBuilder.linkedHashKeys().treeSetValues().build();

    /**
     * Create and inject every Plugin.
     */
    public void injectPlugins( ) {
        try {
            // Iterate Plugin Pool.
            for ( APlugin plugin : this.getABukkit().getPluginHandler().getPluginPool() ) {
                this.handleInjection( plugin );
            }
        } catch ( NoSuchMethodException | InvocationTargetException | IllegalAccessException |
                InstantiationException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Handle Dependency Injection for Plugin.
     * Add injected Instances to Injection Pool.
     *
     * @param plugin To inject.
     * @throws NoSuchMethodException     No empty args Constructor found.
     * @throws IllegalAccessException    No access to create Instance.
     * @throws InvocationTargetException If the Constructor throw an Exception
     * @throws InstantiationException    If class is abstract.
     * @see de.p4skal.abukkit.utils.DIUtil
     */
    private void handleInjection( APlugin plugin ) throws InvocationTargetException, NoSuchMethodException,
            InstantiationException, IllegalAccessException {
        // Get DIUtil.
        final DIUtil diUtil = this.getABukkit().getDiUtil();
        // Create instances of Plugin and add to Creation Pool.
        this.create( plugin );
        // Inject Plugin.
        this.inject( plugin );
        // Iterate and remove injected Instances of Creation Pool.
        for ( AInjection injection : this.getCreationPool().removeAll( plugin ) ) {
            // Add Injection to Injection Pool.
            diUtil.getInjectionPool().put( injection.getAClass(), injection.getInstance() );
        }
    }

    /**
     * Create all Classes for specified Plugin.
     *
     * @param plugin To create Classes for.
     * @throws NoSuchMethodException     No empty args Constructor found.
     * @throws IllegalAccessException    No access to create Instance or value of Field could not be changed.
     * @throws InvocationTargetException If the Constructor throw an Exception
     * @throws InstantiationException    If class is abstract.
     */
    private void create( APlugin plugin ) throws NoSuchMethodException, IllegalAccessException,
            InvocationTargetException, InstantiationException {
        for ( AInjection injection : this.getCreationPool().get( plugin ) ) {
            // Get Constructor.
            Constructor<?> constructor = injection.getAClass().getDeclaredConstructor();
            // Make Constructor accessible.
            constructor.setAccessible( true );
            // Create and set instance.
            injection.setInstance( constructor.newInstance() );
        }
    }

    /**
     * Fill all Fields with created Singletons from the Creation Pool.
     *
     * @param plugin To inject.
     * @throws IllegalAccessException When value of Field could not be changed.
     */
    private void inject( APlugin plugin ) throws IllegalAccessException {
        // Get instance pool.
        final TreeSet<AInjection> instancePool = (TreeSet<AInjection>) this.getCreationPool().get( plugin );

        // Iterate Instance Pool.
        for ( AInjection inject : instancePool ) {
            // Check for Creation process.
            if ( inject.getInstance() == null ) {
                throw new InjectionError( inject );
            }

            // Iterate Fields.
            for ( Field field : inject.getAClass().getDeclaredFields() ) {
                // Make Field accessible.
                field.setAccessible( true );

                // Check if Field has Ignore DI.
                if ( field.isAnnotationPresent( Ignore.class ) ) {
                    continue;
                }

                // Iterate Instance Pool.
                for ( AInjection fInjection : instancePool ) {
                    // Check if Field Type is assignable from iterated Injection Type.
                    if ( field.getType().isAssignableFrom( fInjection.getAClass() ) ) {
                        // Set iterated Instance to Field to.
                        field.set( inject.getInstance(), fInjection.getInstance() );
                        break;
                    }
                }
            }
        }
    }

}
