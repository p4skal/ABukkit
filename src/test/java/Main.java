import de.p4skal.abukkit.annotations.*;
import de.p4skal.abukkit.annotations.utils.IntegerArgument;
import de.p4skal.abukkit.annotations.utils.StringArgument;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * @author p4skal
 */
@Inject
@JavaPlugin( name = "XYPlugin", authors = { "p4skal" } )
public class Main {
    @BukkitPlugin
    private Plugin bukkitPlugin;
    private XYControll xyControll;

    @Listener( event = PlayerJoinEvent.class )
    public void onJoin( PlayerJoinEvent event, XYControll xyControll ) {
        new
                BukkitRunnable() {

                    @Override
                    public void run( ) {

                    }
                }.runTask( this.bukkitPlugin );

    }

    @Command( name = { "test" }, arguments = { StringArgument.class, IntegerArgument.class } )
    public void onTest( Player sender, String arg1, Integer arg2 ) {
    }

    @Command( name = { "test" }, arguments = { StringArgument.class }, permissions = { "command.test" } )
    public void onTest( CommandSender sender, String t, XYControll xyControll ) {

    }

    @OnEnable
    @OnLoad
    public void test( XYControll xyControll ) {

    }

}
